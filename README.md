# 关于本项目
> Crafty 是一个灵活的JavaScript游戏框架，本项目为官网的中文翻译版本，旨在帮助英文不好的同学更好的学习Crafty，欢迎大家踊跃翻译，谢谢！（项目源代码：[http://git.oschina.net/yunzhongyue/Crafty](http://git.oschina.net/yunzhongyue/Crafty "Crafty - JavaScript游戏引擎, HTML5游戏引擎")）

# 项目进度
<table>
<tbody>
	<tr><th>菜单</th><th>章节</th><th>翻译进度</th><th>校对进度</th></tr>
	<tr><td>首页</td><td>首页</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>快速开始</td><td>快速开始</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>文档</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>实体</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>事件</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>组件</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>2D绘图</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>文本</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>键盘</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>鼠标</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>声音</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>精灵</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>场景</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>文档</td><td>游戏循环</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>事件列表</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>核心</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>2D</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>动画</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>资源</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>音频</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>控制器</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>调试</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>事件</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>游戏循环</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>图形</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>输入</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>杂项</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>模型</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>场景</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>舞台</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>工具</td><td>已完成</td><td>未校对</td></tr>
	<tr><td>API</td><td>组件</td><td>已完成</td><td>未校对</td></tr>
</tbody>
</table>

# 发布地址
> 本项目发布在 [http://yunzhongyue.oschina.io/craftyjs](http://yunzhongyue.oschina.io/craftyjs "Crafty - JavaScript游戏引擎, HTML5游戏引擎")，如有需要，欢迎查阅。

![Crafty - JavaScript游戏引擎, HTML5游戏引擎](http://yunzhongyue.oschina.io/craftyjs/images/craftyjs.png)

# 捐赠事宜
> 为鼓励游戏创作，这里的所有捐赠将用作后续游戏创意比赛的奖励。（具体比赛规则、方式和时间整理确定后发布。我对游戏的态度，参见[https://my.oschina.net/dot/blog/1342519](https://my.oschina.net/dot/blog/1342519 "游戏不仅仅是娱乐")）

# 学习交流
> 欢迎大家加入【Web技术交流】群，在这里你可以与大家进行web相关的技术交流，分享经验，推荐软件资料，询问问题或解答疑问等，期待着你的加入哦 ^_^。

![Crafty - JavaScript游戏引擎, HTML5游戏引擎](http://yunzhongyue.oschina.io/craftyjs/images/craftyjs_group.png)